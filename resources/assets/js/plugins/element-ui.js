import Vue from 'vue'
import 'element-ui/lib/theme-chalk/index.css'
import en from 'element-ui/lib/locale/lang/en'

// Full element-ui library
// import Element from 'element-ui'
// Vue.use(Element, { locale: en })

import {
  Input,
  Select,
  Option,
  Button,
  Carousel,
  CarouselItem,
} from 'element-ui'

Vue.component(Input.name, Input)
Vue.component(Button.name, Button)
Vue.component(Option.name, Option)
Vue.component(Select.name, Select)
Vue.component(Carousel.name, Carousel)
Vue.component(CarouselItem.name, CarouselItem)
